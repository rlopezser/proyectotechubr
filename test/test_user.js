'use strict';
var mocha = require ("mocha");
var chai = require ("chai");
var chaihttp = require ("chai-http");

chai.use(chaihttp);
var should = chai.should();

var server = require('../server');

describe ("Test Usuario",
  function() {

    var user = "";
    var token = "";

// Test alta usuario: OK
    it ("Test alta usuario: OK",

      function(done) {
        chai.request('http://localhost:3000')
        .put('/apibr/v1/users')
        .set('content-type', 'application/json')
        .send({
          "email" : "beatriz13@bbva.com",
          "name": "beatriz13",
          "last_name": "sanz",
          "phone": 666666666,
          "pass": "12345678"
        })

        .end(
          function(err,res){
            //console.log("Test Alta Usuario");

            res.should.have.status(200);
            res.should.be.json;
            res.body.msg.should.be.eql("Usuario dado de alta");
            // si devolvemos una variable a validar
            //res.body.should.have.property("_id");
            //user = res.body._id;
            done();
          }
        )
    }
  ),
// Test alta usuario: KO. Duplicado
  it ("Test alta usuario: KO. Duplicado",

    function(done) {
      chai.request('http://localhost:3000')
      .put('/apibr/v1/users')
      .set('content-type', 'application/json')
      .send({
        "email" : "beatriz13@bbva.com",
        "name": "beatriz13",
        "last_name": "sanz",
        "phone": 666666666,
        "pass": "12345678"
      })

      .end(
        function(err,res){
          //console.log("Test Alta Usuario con error");

          res.should.have.status(500);
        //  res.should.be.json;
        //  res.body.msg.should.be.eql("Usuario dado de alta");
          // si devolvemos una variable a validar
          //res.body.should.have.property("_id");
          //user = res.body._id;
          done();
        }
      )
  }
),

// Test login usuario: OK
  it ("Test login usuario: OK",

    function(done) {
      chai.request('http://localhost:3000')
      .post('/apibr/v1/login')
      .set('content-type', 'application/json')
      .send({
        "email" : "beatriz13@bbva.com",
        "password" : "12345678"
      })

      .end(
        function(err,res){
          console.log("Test Alta Usuario con error");

          res.should.have.status(200);
          res.should.be.json;
          res.body.msg.should.be.eql("Login correcto");
          res.body.should.have.property("id");
          res.body.should.have.property("token");
          token = res.body.token;
          user = res.body.id;
          done();
        }
      )
  }
),

// Test login usuario: KO
  it ("Test login usuario: KO. Login incorrecto",

    function(done) {
      chai.request('http://localhost:3000')
      .post('/apibr/v1/login')
      .set('content-type', 'application/json')
      .send({
        "email" : "beatriz13@bbva.com",
        "password" : "12345679"
      })

      .end(
        function(err,res){
          console.log("Test Alta Usuario con error");

          res.should.have.status(200);
          res.should.be.json;
          res.body.msg.should.be.eql("Login correcto");
          res.body.should.have.property("id");
          res.body.should.have.property("token");
          token = res.body.token;
          user = res.body.id;
          done();
        }
      )
  }
)




  }
);
