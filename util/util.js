var jwt = require('jsonwebtoken');
const config = require('../config/entorno/config');
const secret = config.secret;


module.exports = {
  validate: function(req,res,next) {
    console.log("validate");
    var token = req.body.token || req.param('token') || req.headers['x_access_token'];
    console.log (token);
    if (token) {
      // verifies secret and checks exp
      jwt.verify(token, secret, function(err, decoded) {
          //console.log ("decoded: "+decoded);
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {
          console.log("siguiente");
          req.decoded = decoded;
          next();
        }
      });
    } else {
      // if there is no token
      // return an error
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });
    }
  }
}
