var nodemailer = require('nodemailer');
const config = require('../config/entorno/config');

// email sender function
module.exports.sendEmail = function(mailOptions, callback){
// Definimos el transporter
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: config.userGmail,
            pass: config.passGmail
        }
    });

// Enviamos el email
transporter.sendMail(mailOptions, function(error, info){
    if (error){
        callback(err);
    } else {
        console.log("Email sent");
        callback(info);
    }
});
};
