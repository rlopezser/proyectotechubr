const config = require('../config/entorno/config');
const accountDAO = require('../coreDAO/accountsDAO');


module.exports.listAccounts = function (req, res) {
  console.log ("listAccounts");
  var idUser = req.decoded._id;
  var status = 500;
  var response = {
    msg: "Error accediendo a cuentas",
  };
  accountDAO.listAccounts(idUser,function(err, result){
    if (err)
    {
      console.log ("err: "+err);
    }
    else
    {
      if (result!=null)
      {
        status = 200;
        response = result;
      }
      res.status (200);
      res.send(response);
    }
  })
}

module.exports.getAccount = function (req, res) {
  console.log ("getAccount");
  var dataAccount = {
    idUser : req.decoded._id,
    iban : req.params.idAccount
  }
  var response = {
    msg: "Error accediendo a cuenta",
  };
  accountDAO.getAccount(dataAccount,function(err, resultado){
    if (err)
    {
      console.log ("err: "+err);
      res.status(500);
      res.send(response);
    }
    else
    {
      res.status (200);
      res.send(resultado);
    }
  })
}

module.exports.createAccount = function (req, res) {
  console.log ("createAccount");
  var response = {
    msg: "Error dando de alta cuenta"
  };
  var status = 500;
  var idUser = req.decoded._id;
  var nCuenta = parseInt(Math.random()*10000000000000000);
  var newAccount = {
    iban: "ES340182"+nCuenta,
    idUser: idUser,
    balance: req.body.balance,
    currency: req.body.currency,
    alias: req.body.alias
  };
  accountDAO.createAccount(newAccount,function(err, resultado){
    if (err)
    {
      console.log ("err: "+err);
      res.status(500);
      res.send(response);
    }
    else
    {
      res.status (200);
      response.msg="Cuenta dada de alta"
      res.send(response);
    }
  })
}

module.exports.deleteAccount = function (req, res) {
  console.log ("deleteAccount");
  var response = {
    msg: "Error borrando cuenta"
  };
  var status = 500;
  var dataAccount = {
    idUser : req.decoded._id,
    iban : req.params.idAccount
  }
  accountDAO.deleteAccount(dataAccount,function(err, resultado){
    if (err)
    {
      console.log ("err: "+err);
      res.status(500);
      res.send(response);
    }
    else
    {
      res.status (200);
      response.msg="Cuenta borrada"
      res.send(response);
    }
  })
}
