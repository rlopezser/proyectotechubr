const bcrypt = require('bcrypt');
const config = require('../config/entorno/config');
const prop = require('../config/properties');
const jwt = require('jsonwebtoken');
const usersDao = require('../coreDAO/usersDAO');
const secret = config.secret;
const saltRounds = prop.saltRounds;
var objectID = require('mongodb').ObjectID
var fromMail = prop.fromMail;
var subjectMail = prop.subjectMail;
var textMail = prop.textMail;
var mail = require('../util/mail');
var dateDif = require('date-diff');

module.exports.userLogin = function (req, res) {
  var emailUser = req.body.email;
  var passUser = req.body.password;
  var status = 404;
  var response = {
    msg: "Usuario o password incorrectos",
    id: null
  };
  if (emailUser!=null)
  {
    var obj = {
      email: emailUser
    }
    usersDao.getUser(obj,function(err, result){
      if (err)
      {
        console.log ("err: "+err);
        res.status(500);
        res.send(response);
      }
      else
      {
        //console.log ("resultado: "+result);
        if (result != null)
        {
          var passBD = result.pass;
          if (bcrypt.compareSync(passUser, passBD))
          {
            response.msg = "Login correcto";
            response.id = result._id;
            var token = jwt.sign(result, secret, {expiresIn: 3600});
            response.token = token
            status = 200;
          }
        }
        res.status(status);
        res.send(response);
      }
    });
  }
  else {
    res.status(status);
    res.send(response);
  }
}

module.exports.recPass = function (req, res) {
  var emailUser = req.body.email;
  var status = 404;
  var response = {
    msg: "Se ha producido un error"
  };
  var obj = {
    email: emailUser
  }
  console.log ("obj: "+JSON.stringify(obj));
  usersDao.getUser(obj,function(err, result){
    if (err)
    {
      console.log ("err: "+err);
      res.status(500);
      res.send(response);
    }
    else
    {
      if (result != null)
      {
        var newTokenPass = bcrypt.hashSync(emailUser, saltRounds);
        var newValues = {
          recPass : {
            status: 1,
            newTokenPass: newTokenPass,
            updated: new Date()
          }
        }
        var objUser = {
          _id: result._id
        }

        usersDao.modifyUser(objUser,newValues,function(err, result){
          if (err)
          {
            console.log ("err: "+err);
            status = 500;
            response.msg = err;
          }
          else {
            var urlRec = {
              email: emailUser,
              token: newTokenPass
            }
            var urlEnconde = new Buffer (JSON.stringify(urlRec)).toString('base64')
            /*var dataMail = {
                from: 'pruebatechu@gmail.com',
                to: 'rlopezser@gmail.com',
                subject: 'Recuperar Password',
                text: config.urlRecPass+urlEnconde
            };*/

            var dataMail = {
                from: fromMail,
                to: emailUser,
                subject: subjectMail,
                html: textMail + config.urlRecPass+urlEnconde
            };

            mail.sendEmail (dataMail, function(err, info){
              console.log("envio");
            });
            status = 200;
            response.msg = "Paso 1 generado"
          }
          res.status (status);
          res.send(response);
        });
      }
      else {
        res.status(status);
        res.send(response);
      }
    }
  });
}

module.exports.restPass = function (req, res) {
  var token = req.params.id;
  var pass = req.body.password;
  var decoded = new Buffer(token, 'base64').toString('ascii');
  var objDataPass = JSON.parse(decoded);
  var email = objDataPass.email;
  var tokenDecoded = objDataPass.token;
  var status = 500;
  var response = {
    msg: "Error al restaurar password"
  };

  var obj = {
    email: email
  }
  usersDao.getUser(obj,function(err, result){
    if (err)
    {
      console.log ("err: "+err);
      res.status(500);
      res.send(response);
    }
    else
    {
      console.log ("1");
      if (result != null)
      {
              console.log ("2");
        var recPass = result.recPass;
        if (recPass!=null)
        {
                console.log ("3");
          //console.log (recPass.updated);
          var fecha_Actual = new Date();
          var diff = new dateDif(fecha_Actual, recPass.updated);
          console.log (diff.hours());
          if (diff.hours() < 24 )
          {
                  console.log ("4");
            var newValues = {
              "pass": bcrypt.hashSync(pass, saltRounds),
              recPass : null
            }

            var objUser = {
              _id: result._id
            }

            usersDao.modifyUser(objUser,newValues,function(err, result){
              if (err)
              {
                      console.log ("5");
                console.log ("err: "+err);
                status = 500;
                response.msg = err;
              }
              else {
                      console.log ("6");
                status = 200;
                response.msg = "Password actualizada"
              }
              res.status (status);
              res.send(response);
            });
          }
          else {
            response.msg = "El enlace de recuperación ha caducado";
            res.status (500);
            res.send(response);
          }
        }
        else {
          res.status (500);
          response.msg = "No habías iniciado el proceso de recuperar password";
          res.send(response);
        }
      }
      else {
        res.status (500);
        response.msg = "No habías iniciado el proceso de recuperar password";
        res.send(response);
      }
    }
  });
}

module.exports.newUser = function (req, res) {
    console.log ("newUser");
    var response = {
      msg: "Error dando de alta usuario"
    };
    var status = 500;
    var dataUser = {
      "email": req.body.email,
      "name": req.body.name,
      "last_name": req.body.last_name,
      "phone": req.body.phone,
      "pass": bcrypt.hashSync(req.body.pass, saltRounds)
    }
    usersDao.newUser(dataUser,function(err, result){
      if (err)
      {
        console.log ("err: "+err);
      }
      else
      {
        status = 200;
        response.msg = "Usuario dado de alta"
      }
      res.status(status);
      res.send(response);
    });
  }

module.exports.getUser = function (req, res) {
  console.log ("getUser");
  var status = 500;
  var dataUser = null;
  var response = {
    msg: "Error recuperando datos de cliente",
  };
  var idUser = req.decoded._id;
  var id = new objectID (idUser);

  var obj = {
    _id: id
  }
  usersDao.getUser(obj,function(err, result){
    if (err)
    {
      console.log ("err: "+err);
    }
    else {
      if (result!=null)
      {
        status = 200;
        dataUser = {
          email: result.email,
          name: result.name,
          last_name: result.last_name,
          phone: result.phone
        }
      }
    }
    res.status (status);
    res.send(dataUser);
  });
}

module.exports.deleteUser = function (req, res) {
  console.log ("deleteUser");
  var response = {
    msg: "Error borrando usuario"
  };
  var status = 500;
  var idUser = req.decoded._id;
  var id = new objectID (idUser);

  var obj = {
    _id: id
  }
  usersDao.deleteUser(obj,function(err, resultado){
    if (err)
    {
      console.log ("err: "+err);
      res.status(500);
      res.send(response);
    }
    else
    {
      res.status (200);
      response.msg="Usuario borrado"
      res.send(response);
    }
  })
}


module.exports.getUser2 = function (req, res) {
  var token = req.body.token || req.param('token') || req.headers['x-access-token'];
	console.log ("token:" +token);
  console.log ("req.headers:" +JSON.stringify(req.headers));

  console.log ("getUser2");
  var status = 500;
  var dataUser = null;
  var response = {
    msg: "Error recuperando datos de cliente",
  };
  var idUser = req.decoded._id;
  var id = new objectID (idUser);

  var obj = {
    _id: id
  }
  usersDao.getUser(obj,function(err, result){
    if (err)
    {
      console.log ("err: "+err);
    }
    else {
      if (result!=null)
      {
        status = 200;
        dataUser = {
          email: result.email,
          name: result.name,
          last_name: result.last_name,
          phone: result.phone
        }
      }
    }
    res.status (status);
    res.send(dataUser);
  });
}

module.exports.modifyUser = function (req, res) {
  console.log ("modifyUser");
  var status = 500;
  var dataUser = null;
  var response = {
    msg: "Error actualizando datos de cliente",
  };
  var idUser = req.decoded._id;
  var newValues = {
    last_name: req.body.last_name,
    phone: req.body.phone,
    name: req.body.name
  }
  var id = new objectID (idUser);

  var obj = {
    _id: id
  }
  usersDao.modifyUser(obj,newValues,function(err, result){
    if (err)
    {
      console.log ("err: "+err);
    }
    else {
      var status = 200;
      response.msg = "usuario actualizado"
    }
    res.status (status);
    res.send(response);
  });
}


module.exports.userLogout = function (req, res) {
  var token = req.body.token || req.param('token') || req.headers['x-access-token'];
  token==null;
  console.log (token);
}
