const transactionsDAO = require('../coreDAO/transactionsDAO');


module.exports.lisTransactions = function (req, res) {
  console.log ("lisTransactions");
  var iban = req.params.idAccount;
  var status = 500;
  var response = {
    msg: "Error consultado movimientos",
  };
  transactionsDAO.listTransactions(iban,function(err, result){
    if (err)
    {
      console.log ("err: "+err);
    }
    else
    {
      if (result!=null)
      {
        status = 200;
        response = result;
      }
      res.status (status);
      res.send(response);
    }
  })
}

module.exports.getTransaction = function (req, res) {
  console.log ("getTransaction");
  var idTransaction = req.params.idTransaction;
  var status = 500;
  var response = {
    msg: "Error consultado detalle de movimiento",
  };
  transactionsDAO.getTransaction(idTransaction,function(err, result){
    if (err)
    {
      console.log ("err: "+err);
    }
    else
    {
      if (result!=null)
      {
        status = 200;
        response = result;
      }
      res.status (status);
      res.send(response);
    }
  })
}

module.exports.setChategory = function (req, res) {
  console.log ("setChategory");
  var idTransaction = req.params.idTransaction;
  var status = 500;
  var response = {
    msg: "Error actualizando detalle de movimiento",
  };
  var chategory = {
    category: req.body.category
  };
  transactionsDAO.setChategory(idTransaction,chategory, function(err, result){
    if (err)
    {
      console.log ("err: "+err);
    }
    else
    {
      if (result!=null)
      {
        status = 200;
        response.msg= "movimiento actualizado";
      }
      res.status (status);
      res.send(response);
    }
  })
}
