const config = require('../config/entorno/config');
const transferDAO = require('../coreDAO/transfersDAO');


module.exports.createTransfer = function (req, res) {
  console.log ("createTransfer");
  var status = 500;

  // Validar iban destino
  var response = {
    msg: "Error haciendo traspaso",
  };

  var dtoTransfer = {
    debtorAccount: req.body.debtorAccount,
    creditorAccount: req.body.creditorAccount,
    amount: Number(req.body.amount),
    remittanceInformation: req.body.remittanceInformation,
    idUser: req.decoded._id
  }

  transferDAO.createTransfer(dtoTransfer,function(err, resultado){
    if (err)
    {
      console.log ("err: "+err);
      response.msg = err;
    }
    else
    {
      status = 200;
      response.msg="Traspso realizado";
    }
    res.status(status);
    res.send(response);
  })
}

module.exports.createWireTransfer = function (req, res) {
  console.log ("createWireTransfer");
  var status = 500;

  // Validar iban destino
  var response = {
    msg: "Error haciendo transferencias",
  };

  var dtoTransfer = {
    debtorAccount: req.body.debtorAccount,
    creditorAccount: req.body.creditorAccount,
    amount: Number(req.body.amount),
    remittanceInformation: req.body.information,
    idUser: req.decoded._id
  }

  console.log("amount: "+req.body.amount);
  transferDAO.createWireTransfer(dtoTransfer,function(err, resultado){
    if (err)
    {
      console.log ("err: "+err);
      response.msg = err;
    }
    else
    {
      status = 200;
      response.msg="Transferencia realizada";
    }
    res.status(status);
    res.send(response);
  })
}
