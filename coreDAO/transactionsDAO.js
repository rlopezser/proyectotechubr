const mongoDB = require ('mongodb').MongoClient;
const config = require('../config/entorno/config');
const url = config.database;
const properties = require('../config/properties');
const db = properties.database;
const collection = properties.colTransactions;
var objectID = require('mongodb').ObjectID
const coodSpain = properties.coord_spain;

module.exports.listTransactions = function (iban, callback) {
  console.log ("listTransactions");
  var query = {
   iban : iban
 }
 var mysort = { date_transaction: -1 }
  mongoDB.connect(url,function(err, database) {
      if (err)
      {
        database.close();
        callback(err, []);
      }
      else {
        var dbo = database.db(db);
        dbo.collection(collection).find(query).sort(mysort).toArray(function(err, result) {
          database.close();
          callback(err,result);
        });
      }
    });
  }

  module.exports.getTransaction = function (idTransaction, callback) {
    console.log ("getTransaction");
    console.log (idTransaction);
    var id =  new objectID (idTransaction);
    var query = {
     _id : id
   }
    mongoDB.connect(url,function(err, database) {
        if (err)
        {
          database.close();
          callback(err, null);
        }
        else {
          var dbo = database.db(db);
          dbo.collection(collection).findOne(query, function(err, result) {
            database.close();
            callback(err,result);
          });
        }
      });
    }

module.exports.newTransactions = function (dtoTransaction, callback) {
  console.log ("newTransactions");
  dtoTransaction.coord = calcularCoord();
  //conlose.log (JSON.stringify(dtoTransaction));
  mongoDB.connect(url,function(err, database) {
      if (err)
      {
        database.close();
        callback(err, null);
      }
      else {
        var dbo = database.db(db);
        dbo.collection(collection).insertOne(dtoTransaction, function(err, result) {
          database.close();
          callback(err,result);
        });
      }
    });
  }

  module.exports.setChategory = function (idTransaction, newValues, callback) {
    console.log ("setChategory");
    var response = {
      msg: "Error actualizando movimiento",
    };
        console.log ("idTransaction: "+idTransaction);
    var id =  new objectID (idTransaction);
    var query = {
      _id : id
    };
    mongoDB.connect(url,function(err, database) {
      if (err)
      {
        database.close();
        callback(err, null);
      }
      else {
        var dbo = database.db(db);
        dbo.collection(collection).updateOne(query, { $set:newValues}, function(err, result) {
          database.close();
          callback(err,result);
        });
      }
    });
  }

  module.exports.deleteTransactions = function (iban,callback) {
    console.log ("deleteTransactions");
    var response = {
      msg: "Error borrando movimientos",
    };
    var query = {
      iban : iban
    };
    mongoDB.connect(url,function(err, database) {
      if (err)
      {
        database.close();
        callback(err, null);
      }
      else {
        var dbo = database.db(db);
        dbo.collection(collection).deleteMany(query, function(err, result) {
          database.close();
          callback(err,result);
        });
      }
    });
  }

  function calcularCoord (){
    var newCoord = {
      lat: coodSpain.lat + (0.1*Math.random()),
      long: coodSpain.long + (0.1*Math.random())
    }
    return newCoord;
  }
