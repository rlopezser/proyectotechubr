const mongoDB = require ('mongodb').MongoClient;
const config = require('../config/entorno/config');
const trasanctionsDAO = require ('./transactionsDAO');
const accountsDAO = require ('./accountsDAO');
const url = config.database;

function constrTransaction (iban,date_transaction,balance,currency,concept,category,sub_category,detail)
{
  var dtoTrans = {
    iban : iban,
    date_transaction: date_transaction,
    balance: balance,
    currency: currency,
    concept: concept,
    category: category,
    sub_category: sub_category,
    detail:detail
  }
  return dtoTrans;
}

module.exports.createTransfer = function (dtoTransfer, callback) {
  var error = "Error realizando traspaso";
  var debtorAccount = {idUser : dtoTransfer.idUser,iban : dtoTransfer.debtorAccount};
  var creditorAccount = {idUser : dtoTransfer.idUser,iban : dtoTransfer.creditorAccount};

  var amount = Number(dtoTransfer.amount);
  var fecha = new Date();
  var detail = {iban_sender : dtoTransfer.creditorAccount,remittanceInformation: dtoTransfer.remittanceInformation}
  var dataTransactionDebtor = constrTransaction(dtoTransfer.debtorAccount,fecha,amount*(-1),"EUR","Traspaso realizado","ocio","Efectivo - Pagos",detail);
  var dataTransactioncreditor = constrTransaction(dtoTransfer.creditorAccount,fecha, amount,"EUR","Traspaso recibido","ocio","Efectivo - Pagos",detail);
  accountsDAO.getAccount(debtorAccount,function(err,resultado)
  {
    console.log ("createTransfer 5");
    var saldoDebtor = Number(resultado.balance)
    console.log ("saldoDebtor: "+saldoDebtor);
    var newBalanceDebtor = saldoDebtor - amount;
    console.log ("newBalanceDebtor: "+newBalanceDebtor);
    var newvaluesDebtor = {
      balance: newBalanceDebtor
    };
    console.log ("createTransfer 6");
    if (saldoDebtor < amount)
    {
      console.log ("createTransfer 7");
      error = "No tiene saldo disponible";
      callback(error,null);
    }else {
      console.log ("createTransfer creditorAccount:"+JSON.stringify(creditorAccount));
      accountsDAO.getAccount(creditorAccount,function(err,resultado)
      {
        console.log ("createTransfer resultado:"+JSON.stringify(resultado));
          if (resultado !=null)
          {
            var saldoCreditor = Number(resultado.balance)
            console.log ("saldoCreditor:"+saldoCreditor);
            var newBalanceCreditor = saldoCreditor + amount;
            console.log ("newBalanceCreditor:"+newBalanceCreditor);
            var newvaluesCreditor = {
              balance: newBalanceCreditor
            };
            //accountsDAO.updateAccount(debtorAccount, { $set:newvaluesDebtor},function(err,resultado){
            accountsDAO.updateAccount(debtorAccount, newvaluesDebtor,function(err,resultado){
              console.log ("createTransfer resultado2:"+resultado);
              if (err)
              {
                callback(err,null);
              }else {
                accountsDAO.updateAccount(creditorAccount, newvaluesCreditor,function(err,resultado){
                  console.log ("createTransfer resultado3:"+resultado);
                  if (err)
                  {
                    callback(err,null);
                  }else {
                    trasanctionsDAO.newTransactions(dataTransactionDebtor,function(err,resultado){
                      console.log ("createTransfer resultado4:"+resultado);
                      if (err)
                      {
                        callback(err,null);
                      }else {
                        trasanctionsDAO.newTransactions(dataTransactioncreditor,function(err,resultado){
                          console.log ("createTransfer resultado5:"+resultado);
                          if (err)
                          {
                            callback(err,null);
                          }else {
                            callback(err,resultado);
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
          else {
            error = "No es el titular de ambas de cuentas";
            callback (error, null);
          }
        });
      }
    });
}

module.exports.createWireTransfer = function (dtoTransfer, callback) {
  var error = "Error realizando transferencia";
  var dataAccount = {
    idUser : dtoTransfer.idUser,
    iban : dtoTransfer.debtorAccount
  };

  var detail = {iban_sender : dtoTransfer.creditorAccount,remittanceInformation: dtoTransfer.remittanceInformation}
  var amount = Number(dtoTransfer.amount);
  console.log("amount:"+amount);
  accountsDAO.getAccount(dataAccount,function(err,resultado)
  {
    //console.log ("resultado:"+JSON.stringify(resultado));
    if (err || resultado == null)
    {
      console.log(err);
      callback(error,null);
    }
    else{
      var saldo = Number(resultado.balance);
      console.log("saldo: "+saldo);
      if (saldo < amount)
      {
        error = "No tiene saldo disponible";
        callback(error,null);
      }else {
        var newBalance = saldo - amount;
        var newvalues = {
          balance: newBalance
        };
        accountsDAO.updateAccount(dataAccount, newvalues,function(err,resultado){
          if (err)
          {
            callback(err,null);
          }else {
            var fecha = new Date();
            var dataTransaction = constrTransaction(dtoTransfer.debtorAccount,fecha,amount*(-1),"EUR","Transferencia realizada","ocio","Efectivo - Pagos",detail);
            trasanctionsDAO.newTransactions(dataTransaction,function(err,resultado){
              if (err)
              {
                resultado = null;
              }
              callback(err,resultado);
            });
          }
        });
      }
    }
  });
}
