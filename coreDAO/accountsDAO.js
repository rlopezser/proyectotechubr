const mongoDB = require ('mongodb').MongoClient;
const config = require('../config/entorno/config');
const transactionsDAO = require ('./transactionsDAO');
const url = config.database;
const properties = require('../config/properties');
const db = properties.database;
const collection = properties.colAccounts;


/*{ user:userid,
  iban:{$in:[iban1,iban2]}}*/

module.exports.listAccounts = function (idUser, callback) {
  console.log ("listAccounts");
  var query = {
    idUser : idUser
  }
  mongoDB.connect(url,function(err, database) {
      if (err)
      {
        database.close();
        callback(err, null);
      }
      else {
        var dbo = database.db(db);
        dbo.collection(collection).find(query).toArray(function(err, result) {
          database.close();
          callback(err,result);
        });
      }
    });
  }

module.exports.getAccount = function (dataAccount, callback) {
  mongoDB.connect(url,function(err, database) {
      if (err)
      {
        database.close();
        callback(err, null);
      }
      else {
        var dbo = database.db(db);
        dbo.collection(collection).findOne(dataAccount, function(err, result) {
          database.close();
          callback(err,result);
        });
      }
    });
  }

  module.exports.createAccount = function (dataAccount, callback) {
    console.log ("createAccount");
    mongoDB.connect(url,function(err, database) {
      if (err)
      {
        database.close();
        callback(err, null);
      }
      else {
        var dbo = database.db(db);
        dbo.collection(collection).insertOne(dataAccount, function(err, result) {
          database.close();
          callback(err,result);
        });
      }
    });
  }

  module.exports.deleteAccount = function (dataAccount, callback) {
    console.log ("deleteAccount");
    mongoDB.connect(url,function(err, database) {
      if (err)
      {
        database.close();
        callback(err, null);
      }
      else {
        var dbo = database.db(db);
        dbo.collection(collection).deleteOne(dataAccount, function(err, result) {
          if (!err)
          {
            transactionsDAO.deleteTransactions(dataAccount.iban,function(err, resultado){
              callback(err,resultado);
            });
          }else{
            database.close();
            callback(err,result);
          }
        });
      }
    });
  }

  module.exports.updateAccount = function (dataAccount, newValues, callback) {
    console.log ("updateAccount");
    var response = {
      msg: "Error actualizando cuenta",
    };
    var query = {
      idUser : dataAccount.idUser,
      iban : dataAccount.iban
    }
    mongoDB.connect(url,function(err, database) {
      if (err)
      {
        callback(err);
      }
      else {
        var dbo = database.db(db);
        dbo.collection(collection).updateOne(query, { $set:newValues}, function(err, result) {
          database.close();
          callback(err,result);
        });
      }
    });
  }
