const mongoDB = require ('mongodb').MongoClient;
const bcrypt = require('bcrypt');
const config = require('../config/entorno/config');
var jwt = require('jsonwebtoken');
const url = config.database;
const secret = config.secret;
const properties = require('../config/properties');
const db = properties.database;
const collection = properties.colUsers;

const saltRounds = 10;

// nuevo usuario
module.exports.newUser = function (dataUser, callback) {
    mongoDB.connect(url,function(err, database) {
      if (err)
      {
        database.close();
        callback(err, null);
      }else {
        var dbo = database.db(db);
        dbo.collection(collection).insertOne(dataUser, function(err, result) {
          database.close();
          callback(err, result);
        });
      }
    });
  }

  module.exports.deleteUser = function (dataUser, callback) {
    console.log ("deleteUser");
    mongoDB.connect(url,function(err, database) {
      if (err)
      {
        database.close();
        callback(err, null);
      }
      else {
        var dbo = database.db(db);
        dbo.collection(collection).deleteOne(dataUser, function(err, result) {
            database.close();
            callback(err,result);
        });
      }
    });
  }

module.exports.getUser = function (dataUser, callback) {
  mongoDB.connect(url,function(err, database) {
    if (err)
    {
      database.close();
      callback(err, null);
    } else {
      var dbo = database.db(db);
      dbo.collection(collection).findOne(dataUser, function(err, result) {
        console.log("result: "+ result);
        database.close();
        callback(err, result);
      });
    }
  });
}

module.exports.modifyUser = function (dataUser, newValues, callback) {
  mongoDB.connect(url,function(err, database) {
    if (err)
    {
      database.close();
      callback(err, null);
    } else {
      var dbo = database.db(db);
      dbo.collection(collection).updateOne(dataUser, {$set: newValues}, function(err, result) {
        database.close();
        callback(err, result);
      });
    }
  });
}
