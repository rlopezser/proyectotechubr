var express = require ('express');
var app = express();
var bodyParser = require ('body-parser');
app.use(bodyParser.json());
const config = require('./config/entorno/config');
const secret = config.secret;

var userModule = require ('./core/users');
var accountModule = require ('./core/accounts');
var transModule = require ('./core/transactions');
var transferModule = require ('./core/transfers');
var util = require ('./util/util');

var jwt = require('jsonwebtoken');
var apiRoutes = express.Router();

const basePubURL = "/apibr";
const basePriURL = "/apipriv";

var port = process.env.PORT || 3000;
app.listen (port);

apiRoutes.use(function(req, res, next) {

	var token = req.body.token || req.param('token') || req.headers['x-access-token'];
	console.log (token);
	if (token) {
		// verifies secret and checks exp
		jwt.verify(token, secret, function(err, decoded) {
        //console.log ("decoded: "+decoded);
			if (err) {
				return res.json({ success: false, message: 'Failed to authenticate token.' });
			} else {
        //console.log("siguiente");
				req.decoded = decoded;
				next();
			}
		});
	} else {
		// if there is no token
		// return an error
		return res.status(403).send({
			success: false,
			message: 'No token provided.'
		});
	}
});

app.use("/apipriv", apiRoutes);

// Login
app.post (basePubURL+"/v1/login",userModule.userLogin);

// Logout
app.post (basePubURL+"/v1/logout",userModule.userLogout);
//apiRoutes.post ("/v1/logout",userModule.userLogout);

// Nuevo usuario
app.put (basePubURL+"/v1/users",userModule.newUser);

// Consultar usuario
app.get (basePubURL+"/v1/users",util.validate,userModule.getUser);
//app.get (basePubURL+"/v2/users/:id",userModule.getUser2);

// RecuperarPassword
app.post (basePubURL+"/v1/users/recPass",userModule.recPass);

// RestarurarPassword
app.post (basePubURL+"/v1/users/recPass/:id",userModule.restPass);

// Consultar usuario
app.delete (basePubURL+"/v1/users",util.validate,userModule.deleteUser);
//app.get (basePubURL+"/v2/users/:id",userModule.getUser2);

// Gestión de usuario
app.post (basePubURL+"/v1/users",util.validate,userModule.modifyUser);
//apiRoutes.post ("/v1/users/:id",userModule.modifyUser);

// Alta de cuenta
app.put (basePubURL+"/v1/accounts",util.validate,accountModule.createAccount);
//apiRoutes.put ("/v1/users/:id/accounts",accountModule.createAccount);


// Recuperar cuentas
app.get (basePubURL+"/v1/accounts",util.validate, accountModule.listAccounts);
//apiRoutes.get ("/v1/users/:id/accounts",accountModule.listAccounts);

// Recuperar detalle cuenta
app.get (basePubURL+"/v1/accounts/:idAccount",util.validate, accountModule.getAccount);
//apiRoutes.get ("/v1/users/:id/accounts/:idAccount",accountModule.getAccount);

// Baja de cuenta
app.delete (basePubURL+"/v1/accounts/:idAccount",util.validate,accountModule.deleteAccount);
//apiRoutes.delete ("/v1/users/:id/accounts/:idAccount",accountModule.deleteAccount);


// Traspaso
app.post (basePubURL+"/v1/accounts/createTransfer",util.validate,transferModule.createTransfer);
//apiRoutes.post ("/v1/users/:id/accounts/createTransfer",transferModule.createTransfer);

// Transferencia
app.post (basePubURL+"/v1/accounts/createWireTransfer",util.validate, transferModule.createWireTransfer);
//apiRoutes.post ("/v1/users/:id/accounts/createWireTransfer",transferModule.createWireTransfer);

// Recuperar listado de movimientos
app.get (basePubURL+"/v1/accounts/:idAccount/transactions",util.validate, transModule.lisTransactions);
//apiRoutes.get ("/v1/users/:id/accounts/:idAccount/transactions",transModule.lisTransactions);

// Recuperar detalle de movimiento
app.get (basePubURL+"/v1/accounts/:idAccount/transactions/:idTransaction",util.validate,transModule.getTransaction);
//apiRoutes.get ("/v1/users/:id/accounts/:idAccount/transactions/:idTransaction",transModule.getTransaction);

// Recategorizar movimiento
app.post (basePubURL+"/v1/accounts/:idAccount/transactions/:idTransaction",util.validate,transModule.setChategory);
//apiRoutes.post ("/v1/users/:id/accounts/:idAccount/transactions/:idTransaction",transModule.setChategory);
