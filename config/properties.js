module.exports = {
	'colUsers': 'users',
	'colAccounts': 'accounts',
  'colTransactions': 'transactions',
  'database':'databasebr',
  'saltRounds': 10,
	'coord_spain':{
		'lat': 40.4893538,
		'long': -3.6827461
	},
	fromMail:'pruebatechu@gmail.com',
	subjectMail:'Recuperar Password',
	textMail: 'Si quieres restaurar tu password pulsa en el siguiente enlace:<br>'
};
